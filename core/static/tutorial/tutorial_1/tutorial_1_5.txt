Okay, we know our goal, but it's not all fun and games!
<br>
What if the enemy throws a soldier into our territory or what if we throw
a soldier into the enemy territory?
<br>
...
<br>
Well, it turns out a soldier may die, and sending a soldier down to enemy
territory might end up poorly...
<br>
...
<br>
Well, let's send one down there anyway. Like any war, we need to learn how to
fight!
