$.get("/static/tutorial/tutorial_3/tutorial_0.txt", function(txt) {
	$("#tut_3_text").html(txt);
});

var tsumego_id = document.getElementById("tsumego_id");
var tsumego = new WGo.Tsumego(tsumego_id, {
	sgfFile: "/static/tutorial/tutorial_3/tutorial_0.sgf",
	displayHintButton: false,
	//debug: true, /* remove this line hide solution */
});
tsumego.setCoordinates(true);

var section = 0;
var LAST_SECTION = 3;

var checkTimer = setInterval(checkComment,1000);
function checkComment(){
	var text = $('.wgo-tsumego-comment').text();
	if (checkContinue(text)){
		clearInterval(checkTimer);
		if (section === LAST_SECTION){
			console.log(section);
			$("#tut_3_finish").toggle();
		}
		else{
			console.log(section);
			$("#tut_3_button").toggle();
		}
	}
}

$("#tut_3_button").click(function(){
	section++;
	$.get("/static/tutorial/tutorial_3/tutorial_"+section+".sgf", function(sgf) {
		var kifu = WGo.SGF.parse(sgf);
		tsumego.loadKifu(kifu);
	});
	$.get("/static/tutorial/tutorial_3/tutorial_"+section+".txt", function(txt) {
		$("#tut_3_text").html(txt);
	});
	$("#tut_3_button").toggle();
	checkTimer = setInterval(checkComment,1000);
});

$("#tut_3_finish").click(function(){
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	var postdata = {
		'id': 3,
		'csrfmiddlewaretoken': csrftoken
	};

	$.post('/journey/', postdata).done(function(data){
		window.location = '/journey';
	});
});
